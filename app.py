"""Basic boilerplate for dockerized Flask app."""
from flask import Flask
from flask import render_template
from flask import send_from_directory
app = Flask(__name__, static_url_path='/static/')


@app.route('/')
def hello_world():
    """Function to handle requests to /."""
    return 'dockerflask!'

@app.route('/hc')
def health_check():
    """Function to handle requests to /hc."""
    return 'ok!'

@app.route('/template_hello/')
@app.route('/template_hello/<name>')
def template_hello(name=None):
    """test the templates."""
    return render_template('bootstrap_hello.html', name=name)


@app.route('/static/<path:path>')
def send_static(path):
    """Method to handle static files with varied paths."""
    return send_from_directory('static', path)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
